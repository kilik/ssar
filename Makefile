# autogenerated script from KickStarter (ansible)
.PHONY: help status tests fixtures

-include custom.Makefile

# User Id
UNAME = $(shell uname)

ifeq ($(UNAME), Linux)
    UID = $(shell id -u)
else
    UID = 1000
endif

ifneq ("$(wildcard .env)","")
	include .env
	export
endif

INTERACTIVE:=$(shell [ -t 0 ] && echo 1)

ifdef INTERACTIVE
# is a terminal
	TTY_DOCKER=-it
	TTY_COMPOSE=
else
# bash job
	TTY_DOCKER=
	TTY_COMPOSE=-T
endif

COMPOSE=docker-compose

ifneq ("$(wildcard /.dockerenv)","")
	echo "Should not be used inside a container"
	exit 1
else
	COMPOSE_PHP_CMD=$(COMPOSE) exec $(TTY_COMPOSE) -u www-data php
endif

## Display this help text
help:
	$(info ---------------------------------------------------------------------)
	$(info -                        Available targets                          -)
	$(info ---------------------------------------------------------------------)
	@awk '/^[a-zA-Z\-\_0-9]+:/ {                                   \
	nb = sub( /^## /, "", helpMsg );                             \
	if(nb == 0) {                                                \
		helpMsg = $$0;                                             \
		nb = sub( /^[^:]*:.* ## /, "", helpMsg );                  \
	}                                                            \
	if (nb)                                                      \
		printf "\033[1;31m%-" width "s\033[0m %s\n", $$1, helpMsg; \
	}                                                              \
	{ helpMsg = $$0 }'                                             \
	$(MAKEFILE_LIST) | column -ts:

#==============================================================================
# Auto conf
#==============================================================================

.env:
	cp .env.dist .env

.php.env:
	cp .php.env.dist .php.env

compare_conf:
	@./scripts/check-env.sh .env
	@./scripts/check-env.sh .php.env

autoconf: .env .php.env compare_conf

#==============================================================================
# Standard docker dev commands
#==============================================================================

## Pull images used in docker-compose config
pull: autoconf
	docker-compose pull

## Start all the containers
up: autoconf
	docker-compose up -d

## Alias -> up
start: up

## Stop all the containers
stop:
	docker-compose stop

## Stop, then... start
restart: stop start

## Down all the containers
down:
	docker-compose down --remove-orphans --volumes

## Logs for all containers of the project
logs:
	docker-compose logs -tf --tail=1000

## Status of containers
ps:
	docker-compose ps

#==============================================================================
# Interactive shells
#==============================================================================

## Enter interactive shell into php container
php: hooks
	docker-compose exec --user www-data php bash

## Enter interactive shell into nginx container
nginx:
	docker-compose exec nginx sh

## Enter mysql shell (USER=myuser, default root)
mysql:
	docker-compose exec mysql mysql -h mysql -p -u ${USER}

#==============================================================================
# Shortcuts
#==============================================================================

## Symfony cache clear
cc:
	$(COMPOSE_PHP_CMD) ./bin/console cache:clear

## CS Fixer
fix:
# 	$(COMPOSE_PHP_CMD) vendor/bin/php-cs-fixer fix src

## Run phpcsfixer
fix-hook:
# 	@$(COMPOSE_PHP_CMD) vendor/bin/php-cs-fixer fix ./src --dry-run; \
# 	EXIT_CODE=$$?;\
# 	$(COMPOSE_PHP_CMD) vendor/bin/php-cs-fixer fix ./src -v; \
# 	exit $$EXIT_CODE

# Install git hooks
hooks:
#	@ln -s -f "./../../.dev/git/pre-commit" ".git/hooks/pre-commit"

# Just wait php ready
wait:
	@$(COMPOSE) run php echo "Container : php is now ready"

## Upgrade sources + rebuild container + launch migrations
upgrade: pull up wait internal_update front_update

## composer + upgrade (migrations,...)
internal_update:
	$(COMPOSE_PHP_CMD) composer install
	$(COMPOSE_PHP_CMD) ./bin/console doctrine:migrations:migrate -n

## Load fixtures (with migrations)
fixtures:
	$(COMPOSE_PHP_CMD) ./bin/console doctrine:database:drop --force
	$(COMPOSE_PHP_CMD) ./bin/console doctrine:database:create
	$(COMPOSE_PHP_CMD) ./bin/console doctrine:migrations:migrate -n
	$(COMPOSE_PHP_CMD) ./bin/console hautelook:fixtures:load --env=test -n

## Load fixtures (without migrations)
fixtures-no-migrations:
	$(COMPOSE_PHP_CMD) ./scripts/load-fixtures.sh


#==============================================================================
# Front
#==============================================================================
front_update: yarn encore

## yarn install
yarn:
	docker run -it --rm -v $(CURDIR):/app -w /app registry.sfam.ovh/node:10.19-buster yarn install

## Update front dependencies
encore:
	docker run -it --rm -v $(CURDIR):/app -w /app registry.sfam.ovh/node:10.19-buster ./node_modules/.bin/encore dev

## Watch webpack modifications
watch:
	docker run --rm -v $(CURDIR):/app -w /app registry.sfam.ovh/node:10.19-buster ./node_modules/.bin/encore dev --watch

## Stop webpack watcher
watch_stop:
	docker stop encore_watch

#==============================================================================
# Global commands
#==============================================================================

## Launch tests (shortcut)
test:
	make -f test.Makefile test

## Down all stacks (dev, test, ci)
down-all: down
#	make -f test.Makefile down
#	make -f ci.Makefile down
