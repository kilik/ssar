#!/usr/bin/env bash

docker run -it --rm  -u ${UID}:${GID} -w /app --volume $(pwd):/app composer create-project "$@"
