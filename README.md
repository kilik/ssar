# Symfony Start And Run - SSAR

Requirements:
* docker
* docker-compose
* git
* make
* [a reverse proxy](https://github.com/nginx-proxy/nginx-proxy)

Default access:

* [Project](http://project.localhost/)
* [PhpMyAdmin](http://pma.project.localhost/): root:test

## Architecture

This project aims to provide basic services (Php, Nginx, MySQL, PhpMyAdmin) to start a new Symfony based project.

Directories explained

| path     | description                                                 |
|----------|-------------------------------------------------------------|
| /        | SSAR root directory - start and stop docker services        |
| /scripts | common scripts to help developers                           |
| /project | the Symfony Project Directory, where your code is versioned |

You should enter in the Php container (with "make php") to launch Symfony console and composer commands.

## Create a new project (or import)

### Create a new project

```shell
git clone ssar.git my-project
cd my-project
./scripts/create-project.sh symfony/website-skeleton project ^4.4
make pull
make up
make php
# now inside php container
git init .
```

### Or import a new project

```shell
git clone ssar.git my-project
cd my-project
make pull
git clone my-project.git project
make up
```

## Run this project

```shell
make up
```

## Work in a php container

```shell
make php
# and launch commands like git, composer, symfony console, etc...
./bin/console 
```
